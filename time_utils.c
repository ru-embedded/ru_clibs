#include "time_utils.h"

#include <time.h>

#define _GNU_SOURCE
#include <sys/time.h>

int64_t get_time_ms() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

int format_time(int64_t time_ms, char *buffer, size_t size) {
    time_t t = time_ms / 1000;
    struct tm *tm = localtime(&t);
    return strftime(buffer, size, TIME_FORMAT, tm);
}
