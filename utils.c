/*
 *  utils.c
 *
 *  Created on: 15 03 2019
 *  Author: ru
 */
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MODULE_NAME "utils"
// #include "logs.h"

int hex_string_to_int(const char *hex) {
    if (!hex) return 0;
    int val;

    if (sscanf(hex, "0x%x", &val) == 1)
        return val;
    else if (sscanf(hex, "%x", &val) == 1)
        return val;

    return 0;
}

void *memdup(void *src, size_t size) {
    void *aux = calloc(1, size);
    if (!aux) return NULL;
    if (!memcpy(aux, src, size)) free(aux);
    return aux;
}

bool json_get_boolint(json_t *j) {
    if (json_is_boolean(j))
        return json_boolean_value(j);
    else if (json_is_integer(j))
        return json_integer_value(j) != 0;
    else
        return false;
}