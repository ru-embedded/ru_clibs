#ifndef _RU_CLIBS_TIME_UTILS_H_
#define _RU_CLIBS_TIME_UTILS_H_

#include <stdint.h>
#include <stdlib.h>

#ifndef TIME_FORMAT
#define TIME_FORMAT "%F %T"
#endif

int64_t get_time_ms();
int format_time(int64_t time_ms, char *buffer, size_t size);

#endif /* !_RU_CLIBS_TIME_UTILS_H_ */