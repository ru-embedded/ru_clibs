#ifndef _RU_CLIBS_NET_H_
#define _RU_CLIBS_NET_H_

#include <netinet/in.h>

int open_udp_server_socket(int port, struct sockaddr_in *servaddr, int timeout_ms);
int open_udp_client_socket(char *host, int port, struct sockaddr_in *servaddr, int timeout_ms);

#endif /* !_RU_CLIBS_NET_H_ */