#ifndef _RU_CLIBS_GPIO_H_
#define _RU_CLIBS_GPIO_H_

typedef struct gpio_t gpio_t;

gpio_t *gpio_init_pin(int pin);
int gpio_export(gpio_t *gpio);
int gpio_unexport(gpio_t *gpio);
int gpio_set_direction_out(gpio_t *gpio);
int gpio_set_direction_in(gpio_t *gpio);
int gpio_write(gpio_t *gpio, int value);
int gpio_read(gpio_t *gpio);
int gpio_cleanup(gpio_t *gpio);

int gpio_pin_num(const char *pin_name);

#endif /* !_RU_CLIBS_GPIO_H_ */
