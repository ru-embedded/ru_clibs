#include "gpio.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define MODULE_NAME "gpio"
#include "logs.h"

#define BUF_SIZE 64

#define EXPORT 1
#define UNEXPORT 0

#define DIRECTION_OUT 1
#define DIRECTION_IN 0

struct gpio_t {
    int exported;
    int direction;
    int value;
    int pin;
};

gpio_t *gpio_init_pin(int pin) {
    gpio_t *gpio = (gpio_t *)calloc(1, sizeof(*gpio));
    if (gpio == NULL) {
        LOG_E("gpio init fail");
        return NULL;
    }
    gpio->pin = pin;
    gpio->exported = -1;
    gpio->direction = -1;
    gpio->value = -1;
    return gpio;
}

static int _un_export(gpio_t *gpio, int export) {
    int fd;
    char buf[BUF_SIZE];

    if (gpio->exported == export) return gpio->exported;

    fd = open((export == EXPORT) ? "/sys/class/gpio/export" : "/sys/class/gpio/unexport", O_WRONLY);
    if (fd == -1) {
        LOG_E("gpio (un)export file open fail");
        return -2;
    }

    sprintf(buf, "%d", gpio->pin);
    int r = write(fd, buf, strlen(buf));
    close(fd);

    gpio->exported = r > 0 ? export : -1;
    return gpio->exported;
}

static int _set_direction(gpio_t *gpio, int direction) {
    if (gpio->exported != EXPORT) return -3;
    if (gpio->direction == direction) return direction;

    int fd;
    char buf[BUF_SIZE];

    sprintf(buf, "/sys/class/gpio/gpio%d/direction", gpio->pin);
    fd = open(buf, O_WRONLY);
    if (fd == -1) {
        LOG_E("gpio direction file open fail");
        return -2;
    }

    int r;
    if (direction == DIRECTION_OUT) {
        r = write(fd, "out", 3);
    } else {
        r = write(fd, "in", 2);
    }
    close(fd);

    gpio->direction = r > 0 ? direction : -1;

    return gpio->direction;
}

static int _open_value(gpio_t *gpio) {
    int fd;
    char buf[BUF_SIZE];
    sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio->pin);

    fd = open(buf, O_RDWR);
    if (fd < 0) {
        LOG_E("gpio value file open fail");
        return -1;
    }

    return fd;
}

int gpio_export(gpio_t *gpio) { return _un_export(gpio, EXPORT); }

int gpio_unexport(gpio_t *gpio) { return _un_export(gpio, UNEXPORT); }

int gpio_set_direction_out(gpio_t *gpio) { return _set_direction(gpio, DIRECTION_OUT); }

int gpio_set_direction_in(gpio_t *gpio) { return _set_direction(gpio, DIRECTION_IN); }

int gpio_write(gpio_t *gpio, int value) {
    if (gpio->direction != DIRECTION_OUT) {
        LOG_E("gpio write fail: gpio wrong direction");
        return -1;
    }
    int fd = _open_value(gpio);
    lseek(fd, 0, SEEK_SET);
    int r = write(fd, value ? "1" : "0", 1);
    close(fd);
    return r > 0 ? 0 : -1;
}

int gpio_read(gpio_t *gpio) {
    if (gpio->direction != DIRECTION_IN) {
        LOG_E("gpio read fail: gpio wrong direction");
        return -1;
    }
    int fd = _open_value(gpio);
    lseek(fd, 0, SEEK_SET);
    char val;
    int r = read(fd, &val, 1);
    close(fd);
    if (r <= 0) return -1;
    return (val == '1') ? 1 : 0;
}

int gpio_cleanup(gpio_t *gpio) {
    gpio_unexport(gpio);
    free(gpio);
}

int gpio_pin_num(const char *pin_name) {
    if (!pin_name) return -1;
    char port;
    int pin;
    int rc = sscanf(pin_name, "P%c%d", &port, &pin);

    if (rc != 2) {
        LOG_E("can not recognize port name or pin number, pin format: \"P<port><pin num>\"");
        return -2;
    }

    if (pin > 31) {
        LOG_E("wrong pin number: %d", pin);
        return -3;
    }

    if (port > 'Z') port -= 32;

    return (port - 'A') * 32 + pin;
}