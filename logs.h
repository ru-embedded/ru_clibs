#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#if DEBUG
#include "time_utils.h"

static void __log(char *type, const char *fmt, ...) {
    va_list argptr;

    char t[32];

    if (format_time(get_time_ms(), t, sizeof(t)) > 0)
        fprintf(stderr, "[%s][%s] %s: ", type, t, MODULE_NAME);
    else
        fprintf(stderr, "[%s] %s: ", type, MODULE_NAME);

    va_start(argptr, fmt);
    vfprintf(stderr, fmt, argptr);
    va_end(argptr);
    fprintf(stderr, "\n");
    fflush(stderr);
}

#define LOG_I(fmt, ...) __log("I", fmt, ##__VA_ARGS__)
#define LOG_D(fmt, ...) __log("D", fmt, ##__VA_ARGS__)
#define LOG_W(fmt, ...) __log("W", fmt, ##__VA_ARGS__)
#define LOG_E(fmt, ...) __log("E", fmt, ##__VA_ARGS__)

#else

#define LOG_I(fmt, ...) fprintf(stderr, "INFO: " fmt "\n", ##__VA_ARGS__)
#define LOG_D(fmt, ...)
#define LOG_W(fmt, ...)
#define LOG_E(fmt, ...)

#endif