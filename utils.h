/*
 *  utils.h
 *
 *  Created on: 15 03 2019
 *  Author: ru
 */

#ifndef _RU_CLIBS_UTILS_H_
#define _RU_CLIBS_UTILS_H_

#include <jansson.h>
#include <stdbool.h>
#include <stddef.h>

int hex_string_to_int(const char *hex);

void *memdup(void *src, size_t size);

bool json_get_boolint(json_t *j);

#endif  // !_RU_CLIBS_UTILS_H_