/*
 *  mqtt_client.h
 *
 *  Created on: 2019-07-07
 *  Author: rafau
 */

#ifndef _MQTT_CLIENT_H_
#define _MQTT_CLIENT_H_

#include <jansson.h>
#include <stdbool.h>

#define MQTT_DEFAULT_PORT 1883

typedef void (*mqtt_client_on_message_callback)(char *message, int m_len, char *topic, int t_len, void *context);

typedef struct {
    bool enabled;
    char *server;
    int port;
    char *id;
    mqtt_client_on_message_callback callback;
    void *context;
} mqtt_client_config_t;

struct mqtt_client;

struct mqtt_client *mqtt_client_init(mqtt_client_config_t *config);
int mqtt_client_subscribe(struct mqtt_client *mc, char *topic);
void mqtt_client_free(struct mqtt_client *mc);

mqtt_client_config_t *mqtt_client_parse_config(json_t *j);
void mqtt_client_config_free(mqtt_client_config_t *config);

#endif /* !_MQTT_CLIENT_H_ */
