/*
 *  json_tcp/server.c
 *
 *  Created on: 16 06 2019
 *  Author: ru
 */
#include "./server.h"

#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#define MODULE_NAME "json_tcp_server"
#include "ru_clibs/logs.h"
#include "ru_clibs/time_utils.h"
#include "ru_clibs/utils.h"

struct json_tcp_server {
    jts_config_t config;

    pthread_t thread;
    pthread_mutex_t mutex;

    int sigpipe_cnt;

    int socket;
    int client;
    json_t *response;
    bool enabled;
    bool busy;
    bool connected;
};

static int _sigpipe_cnt = 0;

void json_tcp_server_sigpipe_handler(void) { _sigpipe_cnt++; }

static inline void _close_socket(struct json_tcp_server *jts) {
    if (jts->socket >= 0) {
        close(jts->socket);
        jts->socket = -1;
        jts->connected = false;
    } else {
        LOG_E("socket is arleady closed");
    }
}

static int _open_socket(struct json_tcp_server *jts) {
    struct sockaddr_in servaddr;
    // struct timeval timeout;

    jts->socket = socket(AF_INET, SOCK_STREAM, 0);
    if (jts->socket == -1) {
        LOG_E("socket creation failed");
        return 1;
    }

    bzero(&servaddr, sizeof(servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(jts->config.port);

    if ((bind(jts->socket, (struct sockaddr *)&servaddr, sizeof(servaddr))) != 0) {
        // LOG_E("socket bind failed");
        _close_socket(jts);
        return 2;
    }

    LOG_I("on port %d, socket opened", jts->config.port);
    jts->connected = true;

    return 0;
}

static int _handle_connection(struct json_tcp_server *jts) {
    char buff[JSON_TCP_SERVER_MAX_BUF_SIZE];
    char *res;
    int n;

    while (1) {
        bzero(buff, JSON_TCP_SERVER_MAX_BUF_SIZE);

        if (jts->sigpipe_cnt != _sigpipe_cnt) {
            jts->sigpipe_cnt = _sigpipe_cnt;
            return 1;
        }

        n = read(jts->client, buff, sizeof(buff));

        if (n < 0) {
            LOG_E("read client msg error");
            return 2;
        } else if (n == 0) {
            // LOG_D("empty message, ignoring");
            usleep(100);
            continue;
            // return 0;
        }

        LOG_D("(%d) received: %s", jts->config.port, buff);

        jts->config.callback(buff, n, jts->config.context);

        if (json_is_null(jts->response)) {
            LOG_I("response not set");
            continue;
        }

        res = json_dumps(jts->response, JSON_COMPACT);
        if (!res) {
            LOG_W("dumping json response error");
            continue;
        }

        LOG_D("sending %s", res);

        if (write(jts->client, res, strlen(res)) <= 0) {
            LOG_E("socket write error");
            _close_socket(jts);
            free(res);
            return 2;
        }

        free(res);

        if (!jts->enabled) break;
    }

    return 0;
}

static void *_thread_worker(void *arg) {
    struct json_tcp_server *jts = (struct json_tcp_server *)arg;
    LOG_D("json tcp socket worker started (port = %d)", jts->config.port);

    struct sockaddr_in cli;
    socklen_t len;

    while (1) {
        if (!jts->enabled) {
            if (jts->connected) _close_socket(jts);
            sleep(1);
            continue;
        }

        if (!jts->connected) {
            if (_open_socket(jts) != 0) {
                sleep(1);
                continue;
            }
        }

        if ((listen(jts->socket, 5)) != 0) {
            LOG_E("Listen failed");
            _close_socket(jts);
            sleep(1);
            continue;
        }
        LOG_I("server starts listening on port %d", jts->config.port);

        len = sizeof(cli);
        jts->client = accept(jts->socket, (struct sockaddr *)&cli, &len);
        if (jts->client < 0) {
            LOG_E("server acccept failed");
            _close_socket(jts);
            continue;
        }
        LOG_I("server acccept the client");

        if (_handle_connection(jts) != 0) {
            _close_socket(jts);
            continue;
        }
    }

    return arg;
}

struct json_tcp_server *json_tcp_server_init(jts_config_t *config) {
    struct json_tcp_server *jts = (struct json_tcp_server *)calloc(1, sizeof(*jts));
    if (!jts) {
        LOG_E("calloc fail");
        return NULL;
    }

    jts->config.port = config->port;
    jts->config.timeout = config->timeout;
    jts->config.callback = config->callback;
    jts->config.context = config->context;
    jts->enabled = false;
    jts->busy = false;
    jts->sigpipe_cnt = _sigpipe_cnt;

    if (pthread_create(&jts->thread, NULL, _thread_worker, (void *)jts)) {
        LOG_E("thread creating failed");
        goto err;
    }

    pthread_mutex_init(&jts->mutex, NULL);

    return jts;
err:
    json_tcp_server_free(jts);
    return NULL;
}

void json_tcp_server_start(struct json_tcp_server *jts) { jts->enabled = true; }

void json_tcp_server_stop(struct json_tcp_server *jts) { jts->enabled = false; }

void json_tcp_server_set_response(struct json_tcp_server *jts, json_t *response) {
    if (!json_is_null(jts->response)) json_decref(jts->response);
    if (!json_is_null(response)) jts->response = json_deep_copy(response);
}

void json_tcp_server_free(struct json_tcp_server *jts) {
    if (jts) {
        _close_socket(jts);
        pthread_cancel(jts->thread);
        pthread_join(jts->thread, NULL);
        pthread_mutex_destroy(&jts->mutex);

        free(jts);
    }
}

jts_config_t *json_tcp_server_parse_config(json_t *j) {
    jts_config_t *config = (jts_config_t *)calloc(1, sizeof(*config));
    if (!config) {
        LOG_E("calloc fail");
        return NULL;
    }

    memset(config, 0, sizeof(*config));

    config->enabled = json_get_boolint(json_object_get(j, "enabled"));

    json_t *val = json_object_get(j, "port");

    config->port = json_is_integer(val) ? json_integer_value(val) : -1;

    return config;
}