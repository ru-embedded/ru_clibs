/*
 *  json_tcp/client.h
 *
 *  Created on: 02 06 2019
 *  Author: ru
 */

#ifndef _JSON_TCP_CLIENT_H_
#define _JSON_TCP_CLIENT_H_

#include <jansson.h>
#include <stdbool.h>
#include <stddef.h>

#define JSON_TCP_CLIENT_MAX_BUF_SIZE 4096

#define JSON_TCP_CLIENT_MSG_ERROR 1
#define JSON_TCP_CLIENT_THREAD_BUSY 2

typedef void (*json_tcp_client_callback)(char *response, int size, void *context);

typedef struct {
    bool enabled;
    char *ip;
    int port;
    int64_t timeout;
    json_tcp_client_callback callback;
    void *context;
} jtc_config_t;

struct json_tcp_client;

struct json_tcp_client *json_tcp_client_init(jtc_config_t *config);
int json_tcp_client_send(struct json_tcp_client *jtc, json_t *message);
bool json_tcp_client_is_busy(struct json_tcp_client *jtc);
bool json_tcp_client_is_connected(struct json_tcp_client *jtc);
void json_tcp_client_free(struct json_tcp_client *jtc);

void json_tcp_client_sigpipe_handler(void);

jtc_config_t *json_tcp_client_parse_config(json_t *j);
void json_tcp_client_config_free(jtc_config_t *config);

#endif  // !_JSON_TCP_CLIENT_H_