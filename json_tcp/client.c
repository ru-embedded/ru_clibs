/*
 *  json_tcp/client.c
 *
 *  Created on: 02 06 2019
 *  Author: ru
 */
#include "./client.h"

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>

#define MODULE_NAME "json_tcp_client"
#include "ru_clibs/logs.h"
#include "ru_clibs/time_utils.h"
#include "ru_clibs/utils.h"

struct json_tcp_client {
    jtc_config_t config;

    pthread_t thread;
    pthread_mutex_t mutex;
    pthread_cond_t cond;

    int sigpipe_cnt;
    int socket;
    json_t *message;

    bool busy;
    bool connected;
};

static int _sigpipe_cnt = 0;

void json_tcp_client_sigpipe_handler(void) { _sigpipe_cnt++; }

static inline void _close_socket(struct json_tcp_client *jtc) {
    if (jtc->socket > 0) {
        close(jtc->socket);
        jtc->socket = -1;
        jtc->connected = false;
    } else {
        LOG_E("%s:%d socket is arleady closed", jtc->config.ip, jtc->config.port);
    }
}

static int _open_socket(struct json_tcp_client *jtc) {
    struct sockaddr_in servaddr;
    struct timeval timeout;
    jtc->socket = socket(AF_INET, SOCK_STREAM, 0);
    if (jtc->socket == -1) {
        LOG_E("%s:%d socket creation failed", jtc->config.ip, jtc->config.port);
        return 1;
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(jtc->config.ip);
    servaddr.sin_port = htons(jtc->config.port);

    if (connect(jtc->socket, (struct sockaddr *)&servaddr, sizeof(servaddr)) != 0) {
        // LOG_W("connection with the server failed %s:%d", jtc->config.ip, jtc->config.port);
        _close_socket(jtc);
        return 2;
    }

    timeout.tv_sec = 0;
    timeout.tv_usec = 100000;  // 100 ms

    if (setsockopt(jtc->socket, SOL_SOCKET, SO_SNDTIMEO, (struct timeval *)&timeout, sizeof(struct timeval)) != 0) {
        LOG_W("%s:%d seting sock options failed", jtc->config.ip, jtc->config.port);
        _close_socket(jtc);
        return 2;
    }

    LOG_I("%s:%d connected", jtc->config.ip, jtc->config.port);
    jtc->connected = true;
    return 0;
}

static void *_thread_worker(void *arg) {
    struct json_tcp_client *jtc = (struct json_tcp_client *)arg;
    LOG_I("%s:%d worker start", jtc->config.ip, jtc->config.port);

    char *msg;
    int rc;
    int64_t send_time;

    while (1) {
        if (!jtc->connected) {
            if (_open_socket(jtc) != 0) {
                usleep(5000);
                continue;
            }
        }

        jtc->busy = false;
        pthread_mutex_lock(&jtc->mutex);  // wait for trigger
        pthread_cond_wait(&jtc->cond, &jtc->mutex);
        pthread_mutex_unlock(&jtc->mutex);
        jtc->busy = true;

        if (jtc->sigpipe_cnt != _sigpipe_cnt) {
            jtc->sigpipe_cnt = _sigpipe_cnt;
            _close_socket(jtc);
            continue;
        }

        msg = json_dumps(jtc->message, JSON_COMPACT);
        if (!msg) {
            LOG_E("%s:%d dumping json message error", jtc->config.ip, jtc->config.port);
            jtc->config.callback(NULL, -1, jtc->config.context);
            continue;
        }

        LOG_I("%s:%d sending to %s", jtc->config.ip, jtc->config.port, msg);

        if (write(jtc->socket, msg, strlen(msg)) <= 0) {
            LOG_E("%s:%d socket write error", jtc->config.ip, jtc->config.port);
            jtc->config.callback(NULL, -1, jtc->config.context);
            _close_socket(jtc);
            continue;
        }

        send_time = get_time_ms();
        // free(msg);
        // msg = (char *)calloc(1, JSON_TCP_CLIENT_MAX_BUF_SIZE);
        msg = (char *)realloc(msg, JSON_TCP_CLIENT_MAX_BUF_SIZE);
        if (!msg) {
            LOG_E("%s:%d not enough memory for response", jtc->config.ip, jtc->config.port);
            jtc->config.callback(NULL, -1, jtc->config.context);
        }

        while (1) {
            rc = read(jtc->socket, msg, JSON_TCP_CLIENT_MAX_BUF_SIZE);

            if (rc < 0) {
                LOG_I("read ret: %d", rc);
                _close_socket(jtc);
                break;
            } else if (rc == 0) {
                if (get_time_ms() - send_time > jtc->config.timeout) break;
                continue;
            }

            break;
        }

        jtc->config.callback(msg, rc, jtc->config.context);
        free(msg);
    }

    return arg;
}

struct json_tcp_client *json_tcp_client_init(jtc_config_t *config) {
    struct json_tcp_client *jtc = (struct json_tcp_client *)calloc(1, sizeof(*jtc));
    if (!jtc) {
        LOG_E("calloc fail");
        return NULL;
    }

    if (config->ip == NULL) {
        LOG_E("invalid ip");
        goto err;
    }

    LOG_I("%s:%d socket init", config->ip, config->port);

    jtc->config.ip = strdup(config->ip);
    jtc->config.port = config->port;
    jtc->config.timeout = config->timeout;
    jtc->config.callback = config->callback;
    jtc->config.context = config->context;
    jtc->socket = -1;
    jtc->sigpipe_cnt = _sigpipe_cnt;

    jtc->busy = false;
    jtc->connected = false;

    if (pthread_create(&jtc->thread, NULL, _thread_worker, jtc)) {
        LOG_E("thread creating failed");
        goto err;
    }

    pthread_mutex_init(&jtc->mutex, NULL);
    pthread_cond_init(&jtc->cond, NULL);

    return jtc;
err:
    json_tcp_client_free(jtc);
    return NULL;
}

int json_tcp_client_send(struct json_tcp_client *jtc, json_t *message) {
    if (!jtc) return 1;

    if (json_is_null(message)) return JSON_TCP_CLIENT_MSG_ERROR;

    if (!jtc->connected) LOG_I("%s:%d is not connected", jtc->config.ip, jtc->config.port);

    if (jtc->busy) {
        LOG_I("%s:%d thread busy", jtc->config.ip, jtc->config.port);
        return JSON_TCP_CLIENT_THREAD_BUSY;
    }

    if (!json_is_null(jtc->message)) json_decref(jtc->message);
    jtc->message = json_deep_copy(message);

    // pthread_mutex_lock(&jtc->mutex);
    pthread_cond_signal(&jtc->cond);
    // pthread_mutex_unlock(&jtc->mutex);

    return 0;
}

bool json_tcp_client_is_busy(struct json_tcp_client *jtc) { return jtc->busy; }

bool json_tcp_client_is_connected(struct json_tcp_client *jtc) { return jtc->connected; }

void json_tcp_client_free(struct json_tcp_client *jtc) {
    if (jtc) {
        _close_socket(jtc);
        pthread_cancel(jtc->thread);
        pthread_join(jtc->thread, NULL);
        pthread_mutex_destroy(&jtc->mutex);
        pthread_cond_destroy(&jtc->cond);

        if (jtc->config.ip) free(jtc->config.ip);

        free(jtc);
    }
}

jtc_config_t *json_tcp_client_parse_config(json_t *j) {
    jtc_config_t *config = (jtc_config_t *)calloc(1, sizeof(*config));
    if (!config) {
        LOG_E("calloc fail");
        return NULL;
    }

    memset(config, 0, sizeof(*config));

    config->enabled = json_get_boolint(json_object_get(j, "enabled"));

    json_t *val = json_object_get(j, "port");

    config->port = json_is_integer(val) ? json_integer_value(val) : -1;

    val = json_object_get(j, "ip");
    if (json_is_string(val)) config->ip = strdup(json_string_value(val));

    return config;
}

void json_tcp_client_config_free(jtc_config_t *config) {
    if (config) {
        if (config->ip) free(config->ip);
        free(config);
    }
}