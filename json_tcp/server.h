/*
 *  json_tcp/server.h
 *
 *  Created on: 16 06 2019
 *  Author: ru
 */
#ifndef _JSON_TCP_SERVER_H_
#define _JSON_TCP_SERVER_H_

#include <jansson.h>
#include <stdbool.h>
#include <stddef.h>

#define JSON_TCP_SERVER_MAX_BUF_SIZE 4096

typedef void (*json_tcp_server_callback)(char *message, int size, void *context);

typedef struct {
    bool enabled;
    int port;
    int64_t timeout;
    json_tcp_server_callback callback;
    void *context;
} jts_config_t;

struct json_tcp_server;

struct json_tcp_server *json_tcp_server_init(jts_config_t *config);
void json_tcp_server_start(struct json_tcp_server *jts);
void json_tcp_server_stop(struct json_tcp_server *jts);
void json_tcp_server_set_response(struct json_tcp_server *jts, json_t *response);
void json_tcp_server_free(struct json_tcp_server *jts);

void json_tcp_server_sigpipe_handler();

jts_config_t *json_tcp_server_parse_config(json_t *j);

#endif  // !_JSON_TCP_SERVER_H_
