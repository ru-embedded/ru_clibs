/*
 *  mqtt_client.c
 *
 *  Created on: 2019-07-07
 *  Author: rafau
 */

#include "mqtt_client.h"

#include <MQTTClient.h>
#include <stdio.h>
#include <stdlib.h>

#define MODULE_NAME "mqtt_client"
#include "logs.h"
#include "utils.h"

struct mqtt_client {
    MQTTClient handle;
    MQTTClient_connectOptions *conn_opts;

    mqtt_client_config_t config;
};

int _on_message(void *context, char *topicName, int topicLen, MQTTClient_message *message) {
    struct mqtt_client *mc = (struct mqtt_client *)context;

    LOG_D("received :%s %*s\n", topicName, message->payloadlen, message->payload);

    if (mc->config.callback != NULL) {
        char *msg = (char *)malloc(message->payloadlen + 1);
        if (msg) {
            memcpy(msg, message->payload, message->payloadlen);
            msg[message->payloadlen] = '\0';
            mc->config.callback(msg, message->payloadlen, topicName, strlen(topicName), mc->config.context);
            free(msg);
        } else {
            LOG_W("malloc failed! on message callback not send");
        }

    } else {
        LOG_I("on message callback not set");
    }

    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

void _publish(MQTTClient client, char *topic, char *payload) {
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    pubmsg.payload = payload;
    pubmsg.payloadlen = strlen(pubmsg.payload);
    pubmsg.qos = 2;
    pubmsg.retained = 0;
    MQTTClient_deliveryToken token;
    MQTTClient_publishMessage(client, topic, &pubmsg, &token);
    MQTTClient_waitForCompletion(client, token, 1000L);
    LOG_I("Message '%s' with delivery token %d delivered\n", payload, token);
}

struct mqtt_client *mqtt_client_init(mqtt_client_config_t *config) {
    struct mqtt_client *mc = (struct mqtt_client *)calloc(1, sizeof(*mc));

    if (!mc) {
        LOG_E("calloc fail, not enouhg memory");
        return NULL;
    }

    mc->config.server = strdup(config->server);
    mc->config.id = strdup(config->id);
    mc->config.port = (config->port > 0) ? config->port : MQTT_DEFAULT_PORT;
    mc->config.callback = config->callback;
    mc->config.context = config->context;

    MQTTClient_create(&mc->handle, mc->config.server, mc->config.id, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    mc->conn_opts = (MQTTClient_connectOptions *)calloc(1, sizeof(*mc->conn_opts));
    if (!mc->conn_opts) {
        LOG_E("calloc fail, not enouhg memory");
        mqtt_client_free(mc);
        return NULL;
    }

    memcpy(mc->conn_opts, &conn_opts, sizeof(*mc->conn_opts));

    MQTTClient_setCallbacks(mc->handle, (void *)mc, NULL, _on_message, NULL);

    return mc;
}

int mqtt_client_subscribe(struct mqtt_client *mc, char *topic) {
    if (!MQTTClient_isConnected(mc->handle)) {
        if (MQTTClient_connect(mc->handle, mc->conn_opts)) {
            LOG_W("Failed to connect");
            mqtt_client_free(mc);
            return -1;
        }
    }

    return MQTTClient_subscribe(mc->handle, topic, 0);
}

void mqtt_client_free(struct mqtt_client *mc) {
    if (mc) {
        if (mc->config.server) free(mc->config.server);
        if (mc->config.id) free(mc->config.id);
        if (mc->conn_opts) free(mc->conn_opts);

        MQTTClient_disconnect(mc->handle, 1000);
        MQTTClient_destroy(&mc->handle);

        free(mc);
    }
}

mqtt_client_config_t *mqtt_client_parse_config(json_t *j) {
    mqtt_client_config_t *config = (mqtt_client_config_t *)calloc(1, sizeof(*config));
    if (!config) {
        LOG_E("calloc fail");
        return NULL;
    }

    memset(config, 0, sizeof(*config));

    config->enabled = json_get_boolint(json_object_get(j, "enabled"));

    json_t *val = json_object_get(j, "port");
    config->port = json_is_integer(val) ? json_integer_value(val) : -1;

    val = json_object_get(j, "server");
    if (json_is_string(val)) config->server = strdup(json_string_value(val));

    val = json_object_get(j, "id");
    if (json_is_string(val)) config->id = strdup(json_string_value(val));

    return config;
}

void mqtt_client_config_free(mqtt_client_config_t *config) {
    if (config) {
        if (config->server) free(config->server);
        if (config->id) free(config->id);
        free(config);
    }
}