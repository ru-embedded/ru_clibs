#include "net_utils.h"

#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#define MODULE_NAME "net_utils"
#include "logs.h"

int open_udp_server_socket(int port, struct sockaddr_in *servaddr, int timeout_ms) {
    LOG_I("open_udp_server_socket");
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    if (sockfd < 0) {
        LOG_E("open UDP socket failed");
        return -1;
    };

    memset(servaddr, 0, sizeof(*servaddr));

    servaddr->sin_family = AF_INET;
    servaddr->sin_addr.s_addr = INADDR_ANY;
    servaddr->sin_port = htons(port);

    if (bind(sockfd, (struct sockaddr *)servaddr, sizeof(*servaddr)) < 0) {
        LOG_E("bind failed");
        close(sockfd);
        return -1;
    }

    if (timeout_ms > 0) {
        struct timeval timeout;
        timeout.tv_sec = timeout_ms / 1000;
        timeout.tv_usec = (timeout_ms % 1000) * 1000;

        if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0) {
            LOG_E("setsockopt failed\n");
            close(sockfd);
            return -1;
        }
    }

    return sockfd;
}

int open_udp_client_socket(char *host, int port, struct sockaddr_in *servaddr, int timeout_ms) {
    LOG_I("open_udp_client_socket");
    int sockfd = sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    if (sockfd < 0) {
        LOG_E("open UDP socket failed");
        return -1;
    };

    memset(servaddr, 0, sizeof(*servaddr));

    servaddr->sin_family = AF_INET;
    servaddr->sin_port = htons(port);
    servaddr->sin_addr.s_addr = inet_addr(host);

    if (timeout_ms > 0) {
        struct timeval timeout;
        timeout.tv_sec = timeout_ms / 1000;
        timeout.tv_usec = (timeout_ms % 1000) * 1000;

        if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0) {
            LOG_E("setsockopt failed\n");
            close(sockfd);
            return -1;
        }
    }

    return sockfd;
}